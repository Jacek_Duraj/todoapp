import React, { Component } from "react";
import Todolist from "./components/Todolist";
import FormToDo from "./components/form";
import "./App.css";

class App extends Component {
  willWork() {
    console.log("aaaa");
  }
  componentDidMount() {
    setTimeout(() => {
      console.log("I Mounted");
    }, 2000);
  }

  render() {
    return (
      <div className="row mt-5">
        <div className="col-md-4 offset-md-1">
          <h2>Articles</h2>
          <Todolist test="abc" func={this.willWork} />
        </div>
        <div className="col-md-4 offset-md-1">
          <h2>Add a new article</h2>
          <FormToDo />
        </div>
      </div>
    );
  }
}

export default App;
