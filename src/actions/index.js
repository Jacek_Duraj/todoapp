export const AddToDo = todo => ({
  type: "ADD_TO_DO",
  payload: todo
});

export const DeleteToDo = todos => ({
  type: "DELETE_TO_DO",
  payload: todos
});

export const ToggleToDo = id => ({
  type: "TOGGLE_TO_DO",
  payload: id
});
