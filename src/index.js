import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import rootReducer from "./reducers";
import registerServiceWorker from "./registerServiceWorker";
import { createStore } from "redux";
import { AddToDo } from "./actions";

const store = createStore(rootReducer);
window.store = store;
window.addArticle = AddToDo;

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
