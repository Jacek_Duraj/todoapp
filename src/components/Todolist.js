import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { DeleteToDo, ToggleToDo } from "../actions";

const mapStateToProps = (state, props) => {
  return {
    func: props.func,
    todos: state.todosx
  };
};

const mapDispatchToProps = dispatch => {
  return {
    DeleteToDo: filt => dispatch(DeleteToDo(filt)),
    ToggleToDo: id => dispatch(ToggleToDo(id))
  };
};

const Todolist = ({ todos, func, DeleteToDo, ToggleToDo }) => (
  console.log(todos),
  func(),
  (
    <ul className="list-group list-group-flush">
      {todos.map(todo => (
        <div className="item" key={todo.id}>
          <li
            className="list-group-item"
            key={todo.id}
            id={todo.id}
            onClick={() => ToggleToDo(todo.id)}
            style={{
              textDecoration: todo.completed ? "line-through" : "none"
            }}
          >
            {todo.title}
            <button
              id={todo.id}
              onClick={e => {
                const filt = todos.filter(todo => {
                  return e.target.id != todo.id;
                });
                DeleteToDo(filt);
              }}
            >
              X
            </button>
          </li>
        </div>
      ))}
    </ul>
  )
);

const List = connect(
  mapStateToProps,
  mapDispatchToProps
)(Todolist);

Todolist.propTypes = {
  todos: PropTypes.array.isRequired,
  DeleteToDo: PropTypes.func.isRequired,
  ToggleToDo: PropTypes.func.isRequired
};

export default List;
