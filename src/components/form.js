import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import uuidv1 from "uuid";
import { AddToDo } from "../actions";

const mapDispatchToProps = dispatch => {
  return {
    AddToDo: article => dispatch(AddToDo(article))
  };
};

class FormToDo extends Component {
  state = {
    title: ""
  };

  handleChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { title } = this.state;
    const id = uuidv1();
    this.props.AddToDo({ title, id });
    this.setState({ title: "" });
  };
  render() {
    const { title } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            id="title"
            value={title}
            onChange={this.handleChange}
          />
        </div>
        <button type="submit" className="btn btn-success btn-lg">
          SAVE
        </button>
      </form>
    );
  }
}

FormToDo.propTypes = {
  AddToDo: PropTypes.func.isRequired
};

const Form = connect(
  null,
  mapDispatchToProps
)(FormToDo);
export default Form;
