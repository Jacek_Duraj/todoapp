import articleReducer from "./articleReducer";
import { combineReducers } from "../../../.cache/typescript/2.9/node_modules/redux";

const rootReducer = combineReducers({
  todosx: articleReducer
});

export default rootReducer;
