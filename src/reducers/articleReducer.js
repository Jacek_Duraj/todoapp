import { ADD_TO_DO } from "../constants";
import { DELETE_TO_DO } from "../constants";
import { TOGGLE_TO_DO } from "../constants";

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_DO:
      console.log(action);
      return [
        ...state,
        {
          title: action.payload.title,
          id: action.payload.id,
          completed: false
        }
      ];

    case DELETE_TO_DO:
      return action.payload;

    case TOGGLE_TO_DO:
      return state.map(todo => {
        if (todo.id === action.payload) {
          todo.completed = !todo.completed;
        }
        return todo;
      });
    default:
      return state;
  }
};

export default reducer;
